CC := gcc

EXEC := orc
FLAGS := -Wall -lyaml
SRC := $(wildcard src/*.c)
OBJ := $(SRC:src/%.c=obj/%.o)
MAIN_OBJ := $(filter-out obj/*_test.o, $(OBJ))

EXEC_TEST := runner
FLAGS_TEST := $(FLAGS)
OBJ_TEST := $(filter-out obj/$(EXEC).o, $(OBJ))

# -------------------------------------------------------------------

.PHONY: all
all: bin/$(EXEC)

bin/$(EXEC): $(MAIN_OBJ)
	@$(CC) $(FLAGS) $^ -o $@ && echo "[OK] $@"

# -------------------------------------------------------------------

obj/%.o: src/%.c
	@$(CC) $(FLAGS) -c $< -o $@ && echo "[OK] $@"

# -------------------------------------------------------------------

.PHONY: test
test: bin/$(EXEC_TEST)

bin/$(EXEC_TEST): $(OBJ_TEST)
	@$(CC) $(FLAGS_TEST) $^ -o $@ && echo "[OK] $@"
	@./bin/$(EXEC_TEST)

# -------------------------------------------------------------------

.PHONY: clean, clear
clear clean:
	@rm -f obj/* && echo "[CL] obj/"
	@rm -f bin/* && echo "[CL] bin/"
