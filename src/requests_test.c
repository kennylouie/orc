#include <stdio.h>
#include <stdlib.h>

#include "../include/test_macros.h" // _assert, _verify
#include "../include/requests.h" // request, requests

int tests_run = 0;

int request_01() {
	request* r = malloc(sizeof(request));
	r->name = "example_01";
	r->host = "localhost:9200";
	r->endpoint = "/";
	r->body = "";
	r->headers = "";

	_assert(requests(r) == 0);
	free(r);
	return EXIT_SUCCESS;
}

int all_tests() {
	_verify(request_01);
	return EXIT_SUCCESS;
}

int main() {
	int result = all_tests();
	if (result == 0) {
        	printf("PASSED\n");
	}

	printf("Tests run: %d\n", tests_run);

	return result != EXIT_SUCCESS;
}
