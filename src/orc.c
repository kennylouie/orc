#include <stdio.h>
#include <yaml.h>
#include "../include/requests.h"

int main(int argc, char* argv[]) {

	// args parsing
	if (argc < 2) {
		printf("Expected %s yaml file\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	printf("Starting %s program with %s yaml file %s\n", argv[0], argv[0], argv[1]);

	if (argc > 2) {
		int totalUrls = argc - 2;
                printf("Running specifically %d urls\n", totalUrls);
	}

	// yaml parsing
	FILE* fh = fopen(argv[1], "r");
	yaml_parser_t parser;
	yaml_event_t event;

	if (!yaml_parser_initialize(&parser))
		fputs("Failed to initialize parser!\n", stderr);
	if (fh == NULL)
		fputs("Failed to open file!\n", stderr);

	yaml_parser_set_input_file(&parser, fh);

	do {
		if (!yaml_parser_parse(&parser, &event)) {
			printf("Parser error %d\n", parser.error);
			exit(EXIT_FAILURE);
		}

		switch(event.type)
		{
			case YAML_NO_EVENT: puts("Nothing to parse."); break;

			/* Stream start/end */
			case YAML_STREAM_START_EVENT: puts("STREAM START"); break;
			case YAML_STREAM_END_EVENT:   puts("STREAM END");   break;
			/* Block delimeters */
			case YAML_DOCUMENT_START_EVENT: puts("<b>Start Document</b>"); break;
			case YAML_DOCUMENT_END_EVENT:   puts("<b>End Document</b>");   break;
			case YAML_SEQUENCE_START_EVENT: puts("<b>Start Sequence</b>"); break;
			case YAML_SEQUENCE_END_EVENT:   puts("<b>End Sequence</b>");   break;
			case YAML_MAPPING_START_EVENT:  puts("<b>Start Mapping</b>");  break;
			case YAML_MAPPING_END_EVENT:    puts("<b>End Mapping</b>");    break;
			/* Data */
			case YAML_ALIAS_EVENT:  printf("Got alias (anchor %s)\n", event.data.alias.anchor); break;
			case YAML_SCALAR_EVENT: printf("Got scalar (value %s)\n", event.data.scalar.value); break;
		}
		if(event.type != YAML_STREAM_END_EVENT)
			yaml_event_delete(&event);
	} while(event.type != YAML_STREAM_END_EVENT);

	yaml_event_delete(&event);

	/* END new code */
	yaml_parser_delete(&parser);
	fclose(fh);
	return 0;
}
