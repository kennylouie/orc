# Ops REST Console

## Introduction

Simple C cli tool used to make REST commands, i.e. http requests. All requests are created in a yaml file.

## Design

```
 +-------------------------------------------+             +--------------------+
 | orc  +-----------------------------+      |             |                    |
 |      | yaml          request 1  ---+------+------------>|                    |
 |      |               request 2  ---+------+------------>|    External api    |
 |      |               ...        ---+------+------------>|                    |
 |      +-----------------------------+      |             |                    |
 +-------------------------------------------+             +--------------------+
 ```

## Example Yaml Structure
 ```
 # test.yaml

 test_01:
   - host: http://localhost:9200
   - endpoint: /_cat/indices?format=json
   - GET #default method is GET when not specified

#... more requests can be saved to yaml file
 ```

## Usage

This will run every request in the test.yaml
```
orc test.yaml
```

To run specific requests:
```
orc test.yaml request_01
```
