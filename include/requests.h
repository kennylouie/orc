#ifndef __requests_h__
#define __requests_h__

// unit request to be sent
typedef struct {
	char* name;
	char* host;
	char* endpoint;
	char* body;
	char* headers;
} request;

int requests(request* r);

#endif
